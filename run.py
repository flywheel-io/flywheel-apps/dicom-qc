#!/usr/bin/env python
"""The run script."""

import logging
import sys
import typing as t

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_dicom_qc import main, parser, utils

log = logging.getLogger(__name__)


def entry(context: GearToolkitContext) -> t.Optional[int]:  # pragma: no cover
    """Parses config and run."""
    dicom, schema, rules = parser.parse_config(context)

    validation_results, rule_results = main.run(dicom, schema, rules)
    utils.update_metadata(context, rule_results, validation_results)

    # If fail_on_critical_error and job_fail encountered, gear should update
    # metadata but not tag output, so that any further gear rules are not triggered
    if context.config.get("fail_on_critical_error"):
        for res in rule_results:
            if rule_results[res].get("job_fail"):
                log.warning(
                    "Critical error encountered during testing and "
                    "`fail_on_critical_error` is set to True; therefore, "
                    "gear will be marked as unsuccessful."
                )
                return 1

    # get input-file tags
    tag = context.config.get("tag")
    context.metadata.add_file_tags(context.get_input("dicom"), tag)


if __name__ == "__main__":  # pragma: no cover
    # TODO: Change when schema is fixed in core
    with GearToolkitContext(fail_on_validation=False) as gear_context:
        try:
            gear_context.init_logging()
            status = entry(gear_context)
        except Exception as exc:
            log.exception(exc)
            status = 1

    sys.exit(status)

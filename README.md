# DICOM QC gear

dicom-qc is a Flywheel Gear that evaluates basic QC rules against a dicom archive.

## Documentation

The documentation of the gear can be found
[here](https://flywheel-io.gitlab.io/scientific-solutions/gears/dicom-qc/index.html).

## Contributing

Please refer to the CONTRIBUTING.md file for information on how to
contribute to the gear.

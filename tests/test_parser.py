"""Module to test parser.py"""

from unittest.mock import MagicMock

from fw_gear_dicom_qc.parser import parse_config


def test_parse_config(mocker):
    gc = MagicMock()
    gc.get_input.return_value = "dicom"
    gc.config = {
        "test1": True,
        "debug": False,
        "tag": "toto",
        "fail_on_critical_error": True,
    }
    # open_mock = mocker.patch("builtins.open")
    json_mock = mocker.patch("json.load")
    json_mock.return_value = {"test3": "test4"}
    dicom, schema, rules = parse_config(gc)
    assert dicom == "dicom"
    assert schema == {"test3": "test4"}
    assert rules == {"test1": True}

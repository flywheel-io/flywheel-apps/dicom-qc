import logging
from unittest.mock import MagicMock

import pytest
from flywheel_gear_toolkit.testing.files import create_dcm
from flywheel_gear_toolkit.utils.metadata import Metadata
from fw_file.dicom import DICOMCollection

from fw_gear_dicom_qc.utils import check_for_4d, update_metadata

sample_validation_results = [
    {
        "error_type": "anyOf",
        "error_message": "{} is not valid under any of the given schemas",
        "error_value": [
            {"required": ["AcquisitionDate"]},
            {"required": ["SeriesDate"]},
            {"required": ["StudyDate"]},
        ],
        "error_context": [
            "'AcquisitionDate' is a required property",
            "'SeriesDate' is a required property",
            "'StudyDate' is a required property",
        ],
        "item": "file.info.header.dicom",
    }
]

sample_rule_results = {
    "bed_moving": {"state": "PASS", "description": None},
    "embedded_localizer": {"state": "PASS", "description": None},
    "instance_number_uniqueness": {"state": "PASS", "description": None},
    "series_consistency": {"state": "PASS", "description": None},
    "slice_consistency": {"state": "PASS", "description": None},
    "check_zero_byte": {
        "state": "FAIL",
        "description": "Found zero-byte files: \n/tmp/test.dicom.zip",
    },
}


@pytest.fixture
def dcmcoll(tmp_path):
    """Return DICOMCollection with two files."""

    def _gen(**kwargs):
        for key, val in kwargs.items():
            create_dcm(**val, file=str(tmp_path / (key + ".dcm")))
        return DICOMCollection.from_dir(tmp_path)

    return _gen


def test_create_metadata():
    context = MagicMock()
    context.metadata = Metadata(context)
    context.manifest = {"name": "test", "version": "0.1.0"}
    file_ = {
        "location": {"name": "test"},
        "hierarchy": {"type": "acquisition"},
        "object": {"info": {}},
    }
    context.get_input.return_value = file_
    update_metadata(context, sample_rule_results, sample_validation_results)
    qc = context.metadata._metadata["acquisition"]["files"][0]["info"]["qc"]["test"]
    exp = set(["job_info", "jsonschema-validation", *sample_rule_results.keys()])
    assert set(qc.keys()) == exp
    assert qc["jsonschema-validation"]["data"] == sample_validation_results


def test_check_4d_TriggerTime_TemporalPos(dcmcoll, caplog):
    caplog.set_level(logging.INFO)
    num_dicoms = 5
    coll = dcmcoll(
        **{
            f"test{i}": {
                "TriggerTime": i * 1000,
                "NumberOfTemporalPositions": num_dicoms,
                "TemporalPositionIdentifier": i,
            }
            for i in range(num_dicoms)
        }
    )

    res = check_for_4d(coll)
    assert res
    assert f"TriggerTime, {num_dicoms} unique values identified" in caplog.text
    assert f"{num_dicoms} temporal positions" in caplog.text
    assert (
        f"AcquisitionNumber, {num_dicoms} unique values identified" not in caplog.text
    )


@pytest.mark.parametrize(
    "tag",
    [
        "AcquisitionNumber",  # Optional
        "EchoTime",  # MR Required, empty if unknown
        "EchoNumbers",  # Optional
        "InversionTime",  # Conditionally Required, empty if unknown
        "RepetitionTime",  # Conditionally required, empty if unknown
        "TriggerTime",  # Conditionally required, empty if unknown
        "DiffusionBValue",  # Conditionally required
        "DiffusionGradientOrientation",  # Conditionally required
        "DimensionIndexValues",  # Conditionally required, paired with DimensionIndexSequence
    ],
)
def test_check_4d_tags(dcmcoll, caplog, tag):
    caplog.set_level(logging.INFO)
    num_dicoms = 5
    coll = dcmcoll(
        **{
            f"test{i}": {
                tag: i,
            }
            for i in range(num_dicoms)
        }
    )

    res = check_for_4d(coll)
    assert res
    assert f"{tag}, {num_dicoms} unique values identified" in caplog.text


@pytest.mark.parametrize(
    "ipps,expected_res",
    [([[0, 0, i] for i in range(6)], False), ([[0, 0, i] for i in range(3)] * 2, True)],
)
def test_check_4d_ImagePositionPatient(dcmcoll, caplog, ipps, expected_res):
    caplog.set_level(logging.INFO)
    coll = dcmcoll(
        **{
            f"test{i}": {
                "ImagePositionPatient": ipps[i],
            }
            for i in range(len(ipps))
        }
    )

    res = check_for_4d(coll)
    assert res == expected_res
    if expected_res:
        assert "all IPP values are duplicated equally" in caplog.text
    else:
        assert "all IPP values are duplicated equally" not in caplog.text

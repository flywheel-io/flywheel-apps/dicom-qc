# Usages and Workflows

## Synergy with Other Gears

The dicom-qc gear is primarily designed to be used after the
[file-metadata-importer][fmi] gear. This gear extracts metadata from input files
into Flywheel custom information fields. These fields are then parsed and validated
by the dicom-qc gear.

The following other Flywheel gears are often used before dicom-qc:

* dicom-splitter: This gear separates embedded localizers and unique series from
an archive.
* dicom-fixer: This gear attempts to fix invalid values in the dicom and standardizes
the transfer syntax.
* file-classifier: This gear uses templated rules to classify the dicom based on the
extracted header.

## Gear Rules

The dicom-qc gear is also a good candidate to be run as one of a series of gear rules
automatically on file upload ([more here][more]).

[fmi]: https://flywheel-io.gitlab.io/scientific-solutions/gears/file-metadata-importer/
[more]: https://docs.flywheel.io/User_Guides/user_gear_rules/

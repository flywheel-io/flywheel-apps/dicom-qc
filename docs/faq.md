# Frequently Asked Questions

??? info "Can I validate private tags using dicom-qc?"
    Yes! Ensure that you properly extracted private tags into Flywheel custom info
    using the [file-metadata-importer gear][fmi-priv-tags]. After the tag has been
    extracted, you can validate it using dicom-qc. Add the extracted tag as a key
    in the dicom-qc JSON configuration. For example, if you have extracted the CSA
    Image Header Typefrom a Siemens dataset, your configuration might look like this:

    ```json
    {
    "$schema": "https://json-schema.org/draft/2020-12/schema",
    "title": "Dicom header",
    "description": "Dicom header",
    "type": "object",
    "properties": {
        "dicom": {
        "type": "object",
        "properties": {
            "SIEMENS CSA HEADER,0029xx08": {
            "description": "Siemens CSA Image Header Type must be a string",
            "type": "string"
            },
        },
        "required": [
            "SIEMENS CSA HEADER,0029xx08",
        ]
        },
    },
    "required": [
        "dicom",
    ]
    }
    ```

    This configuration validates that the "SIEMENS CSA HEADER,0029xx08" tag is present
    and that its value is a string.

??? info "Does dicom-qc work with dicom-mr-classifier?"
    Yes, you can use dicom-qc if you're utilizing dicom-mr-classifier to extract
    metadata from your files. However, the necessary JSON schema may not match examples
    in this documentation. We recommend using the new pipeline approach, which includes
    dicom-splitter, dicom-fixer, file-metadata-importer, file-classifier, and dicom-qc.

??? info "Where can I see more examples of json validation schema?"
    See examples [here](./examples.md).

??? info "How does dicom-qc handle 4D DICOM files?"
    dicom-qc's test suite is designed to work on a wide variety of DICOM files,
    however the `slice_consistency` and `bed_moving` tests are built for non-4D
    DICOM files. To help determine whether these tests are applicable, the gear
    checks a variety of DICOM tag values and logs findings and recommendations.
    The configuration option `skip_when_4D` controls gear behavior when it encounters
    a 4D DICOM. When `True` (default), the `slice_consistency` and
    `bed_moving` tests are skipped. To configure the gear to not skip these
    tests even if it may be a 4D DICOM, set `skip_when_4D` to `False`.

[fmi-priv-tags]: https://flywheel-io.gitlab.io/scientific-solutions/gears/file-metadata-importer/details/#additonal-dicom-tags
